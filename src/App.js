import './App.css';
import { useState } from 'react'
import 'react-toastify/dist/ReactToastify.css'
import {ToastContainer} from 'react-toastify'
import { toast } from 'react-toastify';

export default function App() {

  const [tarefas, setTarefas] = useState([])
  const [novaTarefa, setNovaTarefa] = useState('')
  const [modal, setModal] = useState(false)


  const abrirModal = () => {
    setModal(!modal)
  }

  function salvarTarefa() {
    if(novaTarefa === "") {
      toast.error('O campo está vazio, adicione uma tarefa')



    } else {
      setTarefas([...tarefas, novaTarefa])
      setNovaTarefa('')
      toast.success('Tarefa Adicionada')
      setModal(false)
    }
  }

  function fecharTarefa() {

   if(novaTarefa === tarefas) {
    alert('esta tarefa...')
    
   } else {
      setModal(false)
    }
  }
    
  return (
    
    <div className="App">

      {
        modal && 
        <div className="modal">
            <div className="modalContent">
                <h3 className="modal-subtitle">Adicionar sua tarefa</h3>
                <input type="text" value={novaTarefa} onChange={evt => setNovaTarefa(evt.target.value)}/>
                <button onClick={salvarTarefa}>Adicionar</button>
                <button style={{backgroundColor: "red"}}onClick={fecharTarefa}>Fechar</button>
            </div>
                
        </div> 
      }

          <div onClick={abrirModal} className="addTarefas">+</div>
            <div className="boxTarefas">
              <h2 style={{textAlign: 'center'}}>Minhas tarefa do dia!</h2>
              <ToastContainer autoClose={3000} />

              {tarefas.map(tarefa => {
                return (
                  <div>
                  <li className="lista-tarefas" key={tarefa.id}>{tarefa}</li>         
                  </div>
                )

              })}
        </div>
    </div>
  );
}


